import React from "react";
import MovieForm from "./components/MovieForm";

function App() {
  return (
    <div className="App">
      <h1>Form</h1> 
      <MovieForm />
    </div>
  );
}

export default App;
