import React from "react";
import "./MovieForm.css";

class MovieForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      poster: "",
      comment: "",
      responseId: ""
    }
  }

  handleSubmit = async (e) => {
    e.preventDefault();
    const url = "http://campus-bordeaux.ovh:3001/api/quests/movies/";
    const options = {
      headers: {
        "Content-Type": "application/json"
      },
      method: "POST",
      body: JSON.stringify(
        {
          "name": this.state.name,
          "poster": this.state.poster,
          "comment": this.state.comment
        }
        )
      };
    try {
      const rawResponse = await fetch(url, options);
      const response = await rawResponse.json();
      this.setState({ responseId: response });
      return 
    } catch(e) {
      console.log(e.message);
    }
  };

  handleChange = (e) => {
    this.setState({
      responseId: "",
      [e.target.name]: e.target.value
    })
  };

  render() {
    const { name, poster, comment, responseId } = this.state;
    return (
      <>
        <form onSubmit={this.handleSubmit}>
          <input type="text" onChange={this.handleChange} name="name" value={name} placeholder="name" required />
          <input type="text" onChange={this.handleChange} name="poster" value={poster} placeholder="poster" required />
          <textarea rows="4" cols="50" onChange={this.handleChange} name="comment" value={comment} placeholder="comment" required></textarea>
          <button typer="submit">Submit</button>  
        </form>
        {responseId && <p>Your movie was saved with the #id {responseId}</p>}
      </>
    )
  }
}

export default MovieForm;